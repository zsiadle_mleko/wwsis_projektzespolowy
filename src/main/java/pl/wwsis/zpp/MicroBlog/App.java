package pl.wwsis.zpp.MicroBlog;
import static spark.Spark.*;

import java.util.HashMap;
import java.util.Map;

import spark.ModelAndView;
import spark.template.freemarker.FreeMarkerEngine;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	get("/:hello", (req, res) -> {
    	Map<String, Object> attributes = new HashMap<>();
    	attributes.put("message", "Hello Wrold!");
    	attributes.put("liczba1", 20);
    	attributes.put("liczba2", 24);
    	
    	return new ModelAndView(attributes, "hello.ftl");
    	
    	}, new FreeMarkerEngine());
    	
    	
//    	get("/hello1", (req,res)-> "Dodatkowy route");
//    	
//    	post("/hello1", (req,res)-> {
//    		return req.body();
//    	});
//    	
//    	get("/private", (req,res)-> {
//    		res.status(401);
//    		return "go away!";
//    	});
//    	
//    	get("/user/:name", (req,res)-> {
//    		return "Selected user" + req.params(":name");
//    	});
//    	
//    	get("/news/:section", (req,res)-> {
//    		res.type("text/html");
//    		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><news><b>" +
//    				req.params("section") + "</b></news>";
//    	});
//    	
//    	get("/protected", (req,res)-> {
//    		halt(403, "I don't think so!!!");
//    		return null;
//    	});
//    	
//    	get("/redirect", (req,res)-> {
//    		res.redirect("/news/world");
//    		return null;
//    
//    	});
//    	
//    	get("/", (req,res)-> {
//    		return "root";
//    	});
    }
}
